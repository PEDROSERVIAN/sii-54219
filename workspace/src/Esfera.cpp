// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	srand(getpid());
	radio=0.75f;
	velocidad.x=rand() % (11) -10;
	velocidad.y=rand() % (11) -10;
	radio_max=1;
	radio_min=0.1;
	pulso=0.3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255, 255);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{

	
	centro=centro+velocidad*t;
	
	if(radio>radio_max){
		pulso=-pulso;
	}
	if(radio<radio_min){
		pulso=-pulso;
	}
	radio=radio+pulso*t;
	
}
