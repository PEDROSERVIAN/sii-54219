// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

//MODIFICADO POR PEDRO J. SERVIAN DIA 08/10

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
