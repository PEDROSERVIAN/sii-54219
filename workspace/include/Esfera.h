// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////

//MODIFICADO POR PEDRO J. SERVIAN DIA 08/10

#if !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Esfera  
{
public:	
	Esfera();
	virtual ~Esfera();
		
	Vector2D centro;
	Vector2D velocidad;
	float radio;
	float pulso;
	float radio_max, radio_min;

	void Mueve(float t);
	void Dibuja();
};

#endif // !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
