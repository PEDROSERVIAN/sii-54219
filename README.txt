El juego se ejecuta desde la carpeta /build/src donde se encuentra el 
ejecutable "tenis". Es un juego para dos personas en un mismo teclado.

Los controles del juego son:
	- 'w': La raqueta de la izquierda se mueve hacia arriba
	- 's': La raqueta de la izquierda se mueve hacia abajo
	- 'o': La raqueta de la derecha se mueve hacia arriba
	- 'l': La raqueta de la derecha se mueve hacia abajo

EL juego consiste en que el que consiga encajar más veces la esfera en la 
línea de fondo del campo contrario gana.
